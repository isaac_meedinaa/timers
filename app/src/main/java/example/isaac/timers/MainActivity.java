package example.isaac.timers;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // runnableDemo();
        countTimeTimerDemo();

    }

    public void runnableDemo() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Hello it's me!", Toast.LENGTH_SHORT).show();
                handler.postDelayed(this, 2000);
            }
        };
        handler.post(runnable);
    }

    public void countTimeTimerDemo() {
        new CountDownTimer(10000, 1000) {

            @Override
            public void onTick(long millisecondsUntilDone) {
                Toast.makeText(MainActivity.this, "Seconds left: " + String.valueOf(millisecondsUntilDone / 1000), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Toast.makeText(MainActivity.this, "We're done!", Toast.LENGTH_SHORT).show();
            }
        }.start();
    }
}
